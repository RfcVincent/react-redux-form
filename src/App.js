import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showForm, hideForm, deleteItem, submitForm } from "./actions";

import { Summary } from './views/summary';
import Form from "./views/form";

import './App.scss';

import { CSSTransition } from 'react-transition-group';

class App extends Component {

  /**
   * Shows the 'Add Item' form
   */
  showAddForm = () => this.props.showForm();

  /**
   * Hides the 'Add Item' form
   */
  hideAddForm = () => this.props.hideForm();

  /**
   * Deletes the specified item from the 'entries'
   * @param index
   */
  deleteItem = index => this.props.deleteItem(index);

  /**
   * Stores the submitted form data in the Redux state
   * @param {object} data - Object containing the submitted data
   */
  submitForm = data => this.props.submitForm(data);

  /**
   * Return true if the value is greater than 1, or zero.
   * @param {number} val - The value to check
   * @returns {boolean}
   **/
  returnIfPluralOrZero = val => val > 1 || !val;

  render() {
    const {entries, formActive} = this.props.reducers;

    return (
      <div className="App">
        <Summary
          pluralCheck={val => this.returnIfPluralOrZero(val)}
          deleteItem={index => this.deleteItem(index)}
          showAddForm={() => this.showAddForm()}
          hideAddForm={() => this.hideAddForm()}
          entries={entries}
          formActive={formActive}
        />
        <CSSTransition
          in={formActive}
          timeout={500}
          classNames="fade-up"
        >
          <Form
            submit={data => this.submitForm(data)}
            hideAddForm={() => this.hideAddForm()}
          />
        </CSSTransition>
      </div>
    );
  }
}

const mapStateToProps = state => ({...state});

const mapDispatchToProps = dispatch => ({
  showForm: () => dispatch(showForm()),
  hideForm: () => dispatch(hideForm()),
  deleteItem: payload => dispatch(deleteItem(payload)),
  submitForm: payload => dispatch(submitForm(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
