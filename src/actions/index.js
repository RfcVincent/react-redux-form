export const showForm = () => dispatch => {
  dispatch({
    type: 'SHOW_FORM',
  })
};

export const hideForm = () => dispatch => {
  dispatch({
    type: 'HIDE_FORM',
  })
};

export const submitForm = payload => dispatch => {
  dispatch({
    type: 'SUBMIT_FORM',
    payload: payload,
  })
};

export const deleteItem = payload => dispatch => {
  dispatch({
    type: 'DELETE_ITEM',
    payload: payload,
  })
};
