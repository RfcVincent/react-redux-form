const initialState = {
  entries: [
    {
      firstName: 'First Name 1',
      lastName: 'Last Name 1',
      email: 'email1@email.com',
    },
    {
      firstName: 'First Name 2',
      lastName: 'Last Name 2',
      email: 'email2@email.com',
      query: 'Query 2',
    }
  ],
  formActive: false,
};

export default (state = {...initialState}, {type, payload}) => {
  switch (type) {
    case 'SHOW_FORM':
      return {
        ...state, formActive: true
      };
    case 'HIDE_FORM':
      return {
        ...state, formActive: false
      };
    case 'SUBMIT_FORM':
      return {
        ...state, entries: [...state.entries, {...payload}]
      };
    case 'DELETE_ITEM':
      const newEntries = state.entries.filter((entry, index) => index !== payload);
      return {
        ...state, entries: [...newEntries]
      };
    default:
      return state
  }
}
