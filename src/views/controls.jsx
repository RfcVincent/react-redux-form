import React from 'react';

export const Controls = props => {
  const {formActive, hideAddForm, showAddForm} = props;

  return (

    <div className="buttons">

      {formActive ? (
        <button
          className="button button--secondary"
          onClick={() => hideAddForm()}
        >
          Close Form
        </button>
      ) : null}

      <button
        className="button button--primary"
        onClick={() => showAddForm()}
        disabled={formActive}
      >
        Add New Item
      </button>

    </div>

  )

};
