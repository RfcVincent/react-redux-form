import React, { Component } from 'react';

export default class Form extends Component {

  constructor(props) {
    super(props);

    this.state = {
      errors: [],
      firstName: '',
      lastName: '',
      email: '',
      query: '',
    }
  }

  /**
   * Handle the updating of all the form fields
   * @param {object} e - The input that has changed
   */
  handleFistNameChange = e => this.setState({firstName: e.target.value});
  handleLastNameChange = e => this.setState({lastName: e.target.value});
  handleEmailChange = e => this.setState({email: e.target.value});
  handleQueryChange = e => this.setState({query: e.target.value});

  /**
   * Handle the form submission
   * @param e
   */
  handleFormSubmit = e => {
    e.preventDefault();
    this.setState({
      errors: [],
    }, this.validateFields);
  };

  /**
   * Validate all fields and push any errors to an array to display to the user. Submit the form data if all fields are validated
   * @returns {boolean}
   */
  validateFields = () => {
    const {firstName, lastName, email, query} = this.state;
    const errors = [];

    if (!firstName) errors.push('Please enter a First Name');
    if (!lastName) errors.push('Please enter a Last Name');
    if (!email) errors.push('Please enter an email address');
    if (!this.validateEmail(email)) errors.push('Please enter a valid email address');

    this.pushError(errors);

    if (!errors.length) {
      this.storeSubmission({firstName: this.capitalise(firstName), lastName: this.capitalise(lastName), email, query});
      this.props.hideAddForm();
      this.resetForm();
    }
  };

  /**
   * Run the method on the parent component to store the submitted data in the Redux state
   * @param {object} data - The object containing the submitted data
   */
  storeSubmission = data => {
    this.props.submit(data);
  };

  /**
   * Resets all form fields and error messages
   */
  resetForm = () => {
    this.setState({
      errors: [],
      firstName: '',
      lastName: '',
      email: '',
      query: '',
    })
  };

  /**
   * Return if supplied string is a valid email address
   * @param {string} e - The email to test
   * @returns {boolean}
   */
  validateEmail = e => {
    const regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regexp.test(e);
  };

  /**
   * Capitalise the first letter of a supplied string
   * @param {string} e - The string to capitalise
   * @returns {string} - The capitalised string
   */
  capitalise = e => e.charAt(0).toUpperCase() + e.slice(1);

  /**
   * Push any error messages to the state
   * @param {array} errors - An array of any errors the form encounters
   */
  pushError = errors => {
    this.setState({
      errors: [...this.state.errors, ...errors]
    })
  };

  render() {

    return (

      <div className="form-container card">

        {
          this.state.errors.length ?
            (
              <div>
                <strong className="text text--error">
                  Error
                </strong>
                <ul className="error-list">
                  {
                    this.state.errors.map((error, index) =>
                      <li key={index}>
                        {error}
                      </li>
                    )
                  }
                </ul>
              </div>
            ) : null
        }

        <form
          noValidate
          className="form"
        >

          <input
            type="text"
            placeholder="First Name"
            value={this.state.firstName}
            onChange={this.handleFistNameChange}
            className="input"
          />
          <input
            type="text"
            placeholder="Last Name"
            value={this.state.lastName}
            onChange={this.handleLastNameChange}
            className="input"
          />
          <input
            type="email"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleEmailChange}
            className="input"
          />
          <textarea
            placeholder="Query"
            value={this.state.query}
            onChange={this.handleQueryChange}
            className="input input--textarea"
          >
          </textarea>

          <button
            type="submit"
            className="button button--primary"
            onClick={e => this.handleFormSubmit(e)}
          >
            Submit
          </button>

        </form>

      </div>

    )

  }

}
