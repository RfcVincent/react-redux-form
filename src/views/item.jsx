import React from 'react';

export const Item = props => {
  return (
    <li className="summary__item">

        <span className="summary__contents">
          <span className="summary__name">
            {props.item.firstName} {props.item.lastName}
          </span>

          <span className="summary__email">
            {props.item.email}
          </span>

          {
            props.item.query ? (
              <span className="summary__query">
                {props.item.query}
              </span>
            ) : null
          }

        </span>

      <button
        className="button button--secondary button--sm summary__delete"
        onClick={() => props.delete(props.index)}
      >
        &times;
      </button>

    </li>
  )
};
