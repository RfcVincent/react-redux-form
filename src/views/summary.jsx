import React from 'react';
import { Item } from './item';
import { Controls } from "./controls";

export const Summary = props => {
  const {entries, pluralCheck, deleteItem, formActive, showAddForm, hideAddForm} = props;
  const entriesLength = entries.length;

  return (

    <div className="summary card">
      <h1 className="summary__title">
        Summary
      </h1>

      <span className="summary__count">
          Found {entriesLength} {pluralCheck(entriesLength) ? 'items' : 'item'}
        </span>

      <ul className="summary__list">
        {
          entries.map((item, index) =>
            <Item
              item={item}
              key={index}
              index={index}
              delete={index => deleteItem(index)}
            />
          )
        }
      </ul>

      <Controls
        formActive={formActive}
        showAddForm={() => showAddForm()}
        hideAddForm={() => hideAddForm()}
      />
    </div>

  )

};
